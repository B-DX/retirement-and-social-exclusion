# Out of society? Retirement affects perceived social exclusion in Germany

This was joined work with [Katharina Mahne](http://mahne-coaching.de/). You find the article [here](http://link.springer.com/article/10.1007/s00391-016-1036-y). was published in *Zeitschrift für Gerontologie und Geriatrie* in 2016.

For an overview of this project [see here](https://b-dx.gitlab.io/post2.html).

### Structure

Data preparation is conducted using stata 14.

1. Request Panel Study Labour Market and Social Security (PASS) data from here: https://fdz.iab.de/de/FDZ_Individual_Data/PASS.aspx

2. Use the "Wetzel-Mahne_ZFGG.do" dofile written in Stata 10 for data preparation as well descriptive (Table 1) and inference analyses (Table).

## If you have questions,

please contact me [here](https://b-dx.gitlab.io/contact.html).

## If you want to use this code:

<a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/"><img alt="Creative Commons License" style="border-width:0" src="https://i.creativecommons.org/l/by-sa/4.0/88x31.png" /></a><br />This work is licensed under a <a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/">Creative Commons Attribution-ShareAlike 4.0 International License</a>.

You are free to:

  * Share — copy and redistribute the material in any medium or format
  * Adapt — remix, transform, and build upon the material
  * for any purpose, even commercially.

This license is acceptable for Free Cultural Works.

  * The licensor cannot revoke these freedoms as long as you follow the license terms.

Under the following terms:

  * Attribution — You must give appropriate credit, provide a link to the license, and indicate if changes were made. You may do so in any reasonable manner, but not in any way that suggests the licensor endorses you or your use.
  * ShareAlike — If you remix, transform, or build upon the material, you must distribute your contributions under the same license as the original.
  * No additional restrictions — You may not apply legal terms or technological measures that legally restrict others from doing anything the license permits.
