clear
set more off
capture log close
clear matrix
clear mata
set maxvar 9000

adopath + " ~ add path here ~ " 
adopath + " ~ add path here ~ "

global mydate: di %tdCYND date(c(current_date), "DMY")

global path_in	   		" ~ add path here ~ "
global path_temp 		" ~ add path here ~ "
global path_log  		" ~ add path here ~ "
global path_out			" ~ add path here ~ "
global path_do			" ~ add path here ~ "
global path_tex			" ~ add path here ~ "
global path_m			" ~ add path here ~ "
global path_g			" ~ add path here ~ "

cap mkdir $path_temp
cap mkdir $path_log
cap mkdir $path_out
cap mkdir $path_tex
cap mkdir $path_m
cap mkdir $path_g

log using "$path_log\ruhestand_ds$mydate.smcl", replace

**********************************************************************************************************************
*
* Wetzel / Mahne 2016 : Out of Working Life, Out of Society? Perceived Social Exclusion due to retirement
*	Data source: PASS 1-7v1
*
* date
di "$path_do -   $mydate    "
*
*
* 0: Merging data sets
* 1: Update time-constant information over waves
* 2: Mean-Imputation of Missings
* 3: Define Settings
* 4: Pathways of Transition
* 5: Compute Retirement Variables and Sample Selection
* 6: Stats for the article
* 7: Fixed-Effect-Regressions


**********************************************************************************************************************
** 0: MERGING DATA SETS	 				                                                              				**
**********************************************************************************************************************
// P-Data loading
global keepvars1 pnr hnr welle fb_vers sample PD0100 
			
do "$path_do/01_keepvars.do"										// contains a global with keepvars

use $keepvars1  $keepvars2 using "$path_in\PENDDAT.dta", clear		// Wave7
numlabel _all, add force det

// Merge with HH-information
do "$path_do/02_hh_infos.do"		// 
merge m:1 hnr welle using "$path_temp\hh.dta" 
keep if _m == 3
drop _merge

// Weight-Infos
merge m:1 hnr welle using "$path_temp\psuinfo.dta" 
keep if _m == 3
drop _merge

merge 1:1 pnr welle using "$path_in\pweights.dta"
keep if _m == 3
drop _merge

// Missing Coding ALL
mvdecode _all, mv(-1=.a \ -2=.b \ -3=.c \ -4=.d \ -5=.e \ -6=.f \ -7=.g \ -8=.h \ -9=.i \ -10=.j)

// Rename 
rename palter alter			// age
rename pintjahr intjahr		// year of interview
rename pintmon intmon		// month of interview
rename depindg2 depr		// deprivation index
rename PSK0200 nwgr			// network size

rename sample stich			// sample
rename bl westost			// region (east/west)

rename kindzges kiza		// no. of children
rename zpsex sex			// sex
rename pnr fallnum			// id

// RECODE education - 3-step from ISCED
bys falln: egen bil3_h = max(isced97)
recode isced97 (1 2 = -1 "-1 low") (3 4 5 = 0 "0 mid") (6 7 8 = 1 "1 high"), gen(bil3)
tab bil3

// COMPUTE PARTNER - Variable
gen part = apart == 1 | epartn == 1
drop apartner epartner

// COMPUTE other Variables
gen ost = westost==2
recode  pintmod (2=1)
recode kiza (0=0) (1 2=1) (3/16=2)

// COMPUTE Volunteering 
d PSK0400*								
egen act = anycount(PSK0400*), v(1)
gen act2 = inrange(act,1,5)
recode act  (0=0) (1/5=1) 

// RECODE Health 
replace PG0300 = 0 if PG0200 == 2
replace PG1100 = - PG1100 + 5
replace PG1200 = - PG1200 + 5
* list fallnum PG0300 if mi(PG0300)

saveold "$path_temp\ret_ds_1.dta", replace
d, s


****************************************************************************************
***! 1: Variables Recoding
****************************************************************************************
use "$path_temp\ret_ds_1.dta", clear

** Updates over waves (time-constant information only in 1st conduction)
******
* EDUCATION
count if bil3 != bil3[_n-1] & fallnum == fallnum[_n-1]
gsort fallnum -welle

tab bil3, m
replace bil3 = bil3[_n-1] if bil3 != bil3[_n-1] & fallnum == fallnum[_n-1]

recode bil3 (-1 0 = 0) (1=1), gen(bil2)
lab def bil2 0 "0: less education" 1 "1: more education", replace
lab val bil2 bil2
tab bil2


* GENDER
sort fallnum welle
count if sex != sex[_n-1] & fallnum == fallnum[_n-1]
gsort fallnum -welle
replace sex = sex[_n-1] if sex != sex[_n-1] & fallnum == fallnum[_n-1]
	
* MIGRATION
gsort fallnum -welle
replace migration = migration[_n-1] if migration != migration[_n-1] & fallnum == fallnum[_n-1]
sort fallnum welle
count if migration != migration[_n-1] & fallnum == fallnum[_n-1]

* NUMBER OF CHILDREN
for numlist 1/5 : replace kiza = kiza[_n+X] if fallnum == fallnum[_n+X] & mi(kiza) & !mi(kiza[_n+X])
for numlist 1/5 : replace kiza = kiza[_n-X] if fallnum == fallnum[_n-X] & mi(kiza) & !mi(kiza[_n-X])

** Outlier
******
replace oecdinc = 10000 if inrange(oecdin,10000,100000)
replace PG0300  =   200 if inrange(PG0300,200,366)
replace PG0100  =    50 if inrange(PG0100,51, 100)
tab kiza

// age-correction
recode alter (54=64) if fallnum == 1001388101 & welle == 4
recode alter (47=60) if fallnum == 1201000301 & welle == 1
recode alter (52=57) if fallnum == 1503795201 & welle == 1

// recode social exclusion
replace PA0800 = - PA0800 + 11
lab def PA0800_lb 1 "1 included" 10 "10 excluded", replace

// logarithm of net equiv income
replace oecdinc = log(oecdinc)
	
* EMPLOYMENT-STATUS
/* tab statakt
           1 1. Erwerbstaetig m. Eink.> 400 Euro/Monat
           2 2. Arbeitslos gemeldet
           3 3. Schueler (bis W3 in Spells n. kat. erh.)
           4 4. Ausbildung/Lehre/Studium
           5 5. Wehr-/Zivildienst/etc.
           6 6. Hausfrau/Hausmann
           7 7. Mutterschutz/Erzieh.urlaub/Elternzeit
           8 8. Rentner/Pensionaer/Vorruhestand
           9 9. Sonstiges/Hauptstatus unklar
          10 10. Arb.los, n. gemeldet (ab W4 aus off. A.)
          11 11. Krank/beruf.unf./erw.unf./beh.(off. A.)
          12 12. Selbst./mithelf. Fam.angeh. (off. A.)		
		  
CAUTION: not generated in 1st wave!
			--> use of 'erwerb' 	*/
tab statakt welle, m			

recode statakt (1 12 =1) (2 3 4 5 6 7 9 10 11=2) (8=3), gen(stat)
lab def stat 1 "working" 2 "not working" 3 "retired", replace
lab val stat stat
* drop statakt

// change in employment variable from wave1 to w2
// GENERATING THE EMPLOYMENT STATUS VARIABLE 
replace stat = 1 if mi(stat) & inlist(erwerb,1) 
replace stat = 2 if mi(stat) & inlist(erwerb,2,3,5,6,7,8,9,10,11,12) 
replace stat = 3 if mi(stat) & inlist(erwerb,4) 

lab def llms 0 "employed" 1 "unemployment" 2 "education" 3 "homemaker" ///
			 4 "invalidity pension" 5 "other" 6 "retired"
tab1 statakt erwerb, m
gen llms = .
replace llms = 0 if inlist(erwerb,1) 		| inlist(statakt,1)
replace llms = 1 if inlist(erwerb,2,3) 		| inlist(statakt,2,10)
replace llms = 2 if inlist(erwerb,8,9,10) 	| inlist(statakt,3,4)
replace llms = 3 if inlist(erwerb,5,6) 		| inlist(statakt,6,7)
replace llms = 4 if inlist(erwerb,11) 		| inlist(statakt,11)
replace llms = 5 if inlist(erwerb,7,12) 	| inlist(statakt,5,9,12)
replace llms = 6 if inlist(erwerb,4) 		| inlist(statakt,8)
lab val llms llms
tab llms , m

/* PROBLEM
CAUTION: PASS does not conduct the employment module after age 65 and
			can therefore not generate statakt after age 65
		--> no Retirement-Date!!!
		--> SOLUTION:   due to legal regulation in germany
						you are retired within one month reaching age 65
						
						      1. Welle   2. Welle   3. Welle   4. Welle   5. Welle   6. Welle  |     Total						
   senior-questionnaire .j |     1,705       1,233      1,335      1,153      1,837      1,824 |     9,087 						

        --> Exception:  Self-employed */

replace stat = 3 if inrange(alter,65,99)

tab stat, m		
		
// FIRST COUNTING of TRANSITIONS 
sort fallnum welle
gen flag = inlist(stat,1,2) & stat[_n+1] == 3 & fallnum == fallnum[_n+1] 
tab flag stat, m
drop flag
* 329 + 850 = 1179

			
saveold "$path_temp\ret_ds_2.dta", replace
d, s

****************************************************************************************
***! 2: Imputation of Missings
****************************************************************************************
use "$path_temp\ret_ds_2.dta", clear
xtset fallnum welle


// Missings Description
egen rm = rowmiss(depr oecd nwgr act PG0100 PG1200)
tab rm
for any stat alter depr oecd nwgr act: tab X if inrange(rm,1,7) & mi(X), m

// Networksize: latest information is used
cap bys fallnum: gen N = _N
for numlist 1/5 : replace nwgr = nwgr[_n+X] if fallnum == fallnum[_n+X] & mi(nwgr) & !mi(nwgr[_n+X])
for numlist 1/5 : replace nwgr = nwgr[_n-X] if fallnum == fallnum[_n-X] & mi(nwgr) & !mi(nwgr[_n-X])
tab nw N, m
replace nwgr = 40 if nwgr > 40 & !mi(nwgr)		
tab nwgr, m
drop N
bys fallnum (welle): gen N = _N

// OECDinc: latest information is used
for numlist 1/5 : replace oecd = oecd[_n+X] if fallnum == fallnum[_n+X] & mi(oecd) & !mi(oecd[_n+X])
for numlist 1/5 : replace oecd = oecd[_n-X] if fallnum == fallnum[_n-X] & mi(oecd) & !mi(oecd[_n-X])

// Age group mean imputation
recode alter (50/54=1) (55/59=2) (60/64=3) (65/73 = 4) (else=.a), gen(a_kat) 
tab a_kat

foreach num of numlist 1/4 {
	qui sum PG0100 if a_kat == `num'
	replace PG0100 = round(`r(mean)',2) if mi(PG0300) 	& a_kat == `num'
	qui sum PG1200 if a_kat == `num'
	replace PG1200 = round(`r(mean)',2) if mi(PG1200)  	& a_kat == `num'
	qui sum nwgr 	if a_kat == `num'
	replace nwgr   = round(`r(mean)',2) if mi(nwgr) 	& a_kat == `num'
	qui sum kiza 	if a_kat == `num'
	replace kiza   = round(`r(mean)',2) if mi(kiza) 	& a_kat == `num'
	qui sum oecd 	if a_kat == `num'
	replace oecd   = round(`r(mean)',2) if mi(oecd)	 	& a_kat == `num'
	qui sum depr 	if a_kat == `num'
	replace depr   = round(`r(mean)',2) if mi(depr)	 	& a_kat == `num'
	}

drop a_kat

****************************************************************************************
***! 3: Define Settings
****************************************************************************************

global gropts ///
	plot1opts(lcolor(gs9) mcolor(gs9) lwidth(thick) ) ci1opts(lcolor(gs9))						///
	plot2opts(lcolor(cranberry) mcolor(cranberry) lwidth(thick)) ci2opts(lcolor(cranberry))						///
	plot3opts(lcolor(black) mcolor(black) lwidth(thick)) ci3opts(lcolor(black))	///	
	legend(row(1) pos(6))  	
global gropts1 ///
	plot1opts(lcolor(black) mcolor(black) lwidth(thick) ) ci1opts(lcolor(black))						///
	legend(row(1) pos(6)) 
global gropts2 ///
	plot1opts(lcolor(black) mcolor(black) lwidth(thick) ) ci1opts(lcolor(black))						///
	plot2opts(lcolor(cranberry) mcolor(cranberry) lwidth(thick)) ci2opts(lcolor(cranberry))						///
	legend(row(1) pos(6))  	
	
global note "PASS W1-6"
set scheme lean2
lab var alter "age"
lab var bil3 "education"
lab var stat "employment status"
lab def stat 1 "working" 2 "not working" 3 "retired", replace


****************************************************************************************
***! 4: PATHWAYS of TRANSITION
****************************************************************************************
*						 wave 1 2 3 4 5 6 7
* ret?   = status-variable    0 0 0 1 1 1 1
* ret?_h = auxillary var      . . . 1 . . .
* ret?_t = transition         0 0 0 1 0 0 0 
* ret?_c = count no. of trans 1 1 1 1 1 1 1

drop N
bys fallnum: gen N = _N
bys fallnum: egen c_empl   = total(stat==1)
bys fallnum: egen c_unempl = total(stat==2)

// Transition from work (stat = 1 --> stat = 3)
************************************************
bys fallnum: egen c_ret1 = total(stat==3)
tab c_ret1
count if c_ret1 == N & fallnum != fallnum[_n+1]
gen 	ret1 = 1 if c_ret1 == N 				// 3,759 individuals premanently retired 
count if c_empl == N  & fallnum != fallnum[_n+1]
replace ret1 = 0 if c_empl == N 				// 8,773 individuals permanently employed
* bro fallnum welle stat ret1* c_ret1* // if unemp_c > 1

sort fallnum welle
gen ret1_h = 1 if stat[_n-1] == 1 & stat == 3 & fallnum == fallnum[_n-1] // Count Transitions
bys fallnum: egen ret1_c = total(ret1_h)
tab ret1_c, m									// 48 pers with 2 Transitions 
egen ret1_t = tag(fallnum ret1_h) 				// only first transition
replace ret1_h = . if ret1_h == 1 & ret1_t != 1
tab ret1_h										// 322 individuals with work --> ret 

bys fallnum (welle): gen n = _n
bys fallnum (welle): replace ret1 = 0 if stat == 1 & stat[_n+1] == 3 & ret1_c != 0 
for any 1 2 3 4 5: /// 		// 5x times loop
bys fallnum (welle): replace ret1 = 0 if ret1[_n+X] == 0 & ret1 == . & stat == 1

replace ret1 = 1 if ret1_h == 1
for any 1 2 3 4 5: /// 		// 5x times loop
	bys fallnum (welle): replace ret1 = 1 if ret1[_n-X] == 1 & ret1 == . & stat == 3


sort fallnum
count if !mi(ret1) & fallnum != fallnum[_n+1]	
count if !mi(ret1)

replace ret1_c = 1 if ret1_c == 0 & stat == 1

*count if ret1 != ret1[_n+1] & fall == falln[_n+1]
*bro falln welle stat ret1* if ret1 != ret1[_n+1] & fall == falln[_n+1]

// finish!							


// Transition from no work (stat = 2 --> stat = 3)
************************************************
bys fallnum: egen c_ret2 = total(stat==3)
tab c_ret2
gen 	ret2 = 0 if c_ret2 == N 
count if c_ret2 == N & fallnum != fallnum[_n+1]
replace	ret2 = 1 if c_ret2 == N 				//  3759 individuals premanently retired
count if c_unempl == N  & fallnum != fallnum[_n+1]
replace ret2 = 0 if c_unempl == N 				// 15345 individuals premanently not working
* bro fallnum welle stat ret2* c_ret2* // if unemp_c > 1

bys fallnum (welle): gen ret2_h = 1 if stat[_n-1] == 2 & stat == 3	// Count Transitions
bys fallnum: egen ret2_c = total(ret2_h)
tab ret2_c, m									// 41 individuals with 2 Transitions 
egen ret2_t = tag(fallnum ret2_h) 				// only first transition
replace ret2_h = . if ret2_h == 1 & ret2_t != 1
tab ret2_h										// 844 individuals with "no work" --> ret 



bys fallnum: replace ret2 = 0 if _n == 1 & stat == 2 & ret2_c != 0
replace ret2 = 1 if ret2_h == 1
bys fallnum (welle): replace ret2 = 1 if ret2[_n-1] == 1 

// cleaning: unempl-spell directly before ret, but not the 1st
replace ret2 = 0 if stat == 2 & ret2 == .  & ret2_c != 0
for any 1 2 3 4 5: /// 		// 5x times loop
	bys fallnum (welle): replace ret2 = 0 if ret2[_n+1] == 0 & ret2 == . & fallnum != X 		
sort fallnum
count if !mi(ret2) & fallnum != fallnum[_n+1]							
count if !mi(ret2)

replace ret2_c = 1 if ret2_c == 0 & stat == 2
// finished!							

count
count if fallnum != fallnum[_n+1]


lab def  ret1 0 "0=working" 1 "1=retired", replace
lab val  ret1   ret1
lab def  ret2 0 "0=not working" 1 "1=retired", replace
lab val  ret2   ret2

for any ret1 ret2: tab X_t
// EVENTS: work --> ret1:  322  
//		no work --> ret2:  844
di 322 + 844

count if alter[_n-1] == 64 & alter == 65 & fallnum == fallnum[_n-1]
// EVENTS: Transition with age 65: 629



****************************************************************************************
***! 5: Compute Retirement Variables and Sample Selection
****************************************************************************************
// GENERATE age at retirement (a_rs)

* bro fallnum welle job intd rsd lewtd a_rs 
* tab a_rs job if fallnum != fallnum[_n+1] , m
bys fallnum: egen a_rs    = total(alter) if ret1_t == 1
bys fallnum: egen a_rs_h  = total(alter) if ret2_t == 1
replace a_rs = a_rs_h if mi(a_rs)
tab a_rs

bys fallnum: egen a_rs2 = total(a_rs)


/* CAUTION: Retirement Transitions in early ages (in the 50th and earlier) may be health related
   SOLUTION: use of an age bracket (58 - 65) 	*/

gen tag = 1 if inrange(a_rs2,58,65) 
drop if tag != 1

for any ret1 ret2: tab X_t
di 919 - 206 - 446

replace ret1 = 0 if !inrange(a_rs2,58,65) & !mi(ret1)
replace ret2 = 0 if !inrange(a_rs2,58,65) & !mi(ret2)

replace ret1_c = 0 if !inrange(a_rs2,58,65) & !mi(ret1)
replace ret2_c = 0 if !inrange(a_rs2,58,65) & !mi(ret2)

replace a_rs = . if !inrange(a_rs2,58,65)
tab a_rs

for any ret1 ret2: tab X_t

/* ONLY EMPLOYEES (delete self-employed) */
tab esec if welle == 1		// cross-sectional w1: 8.4 % self-employed
tab esec statakt, m

sort fallnum welle
drop tag
gen tag = 1 if inlist(esec[_n-1],4,5) & (ret1_t == 1 | ret2_t == 1) & ///
				falln == falln[_n-1]
bys fallnum: egen tag2 = total(tag)
drop if tag2 > 0		// number of self-employed individuals
drop tag tag2			// 133 self-employed

for any ret1 ret2: tab X_t
// EVENTS: work --> ret1:  245  
//		no work --> ret2:  555
di 245 + 555			// 800


// GENERATE Distance of observation to Retirement Date
egen event = rownonmiss(ret1_h ret2_h)
bys fallnum (welle): gen eventy_h = intjahr - 1 if event != 0
bys fallnum (welle): egen eventy = total(eventy_h)

gen dist = intjahr - eventy if eventy != 0
tab dist
lab var dist "distance to last empl status (in yrs)"
replace dist = 0 if inrange(dist,1,5) & !inrange(alter,58,70)


* bro fallnum welle intd rsd lewtd d_rs_m stat job sep dir ind quelle a_rs eb_alt alter 

bys fallnum: egen ret1_c2 = count(ret1)
bys fallnum: egen ret2_c2 = count(ret2)
bys fallnum: egen stat_c2 = count(stat)
for any 1 2 : replace retX_h = . if retX_c == 0
bys fallnum: egen ret1_c3 = count(ret1_h)
bys fallnum: egen ret2_c3 = count(ret2_h)
tab ret1_c3 ret1_h, m

egen ret_miss = rowmiss(PG0300 PG1200 nwgr kiza oecd) if ret1_c3 == 1 | ret2_c3 == 1
for any PG0300 PG1200 nwgr kiza oecd: tab X if inrange(ret_miss,1,3) & mi(X), m

* MERGING the two pathways into the same data-structure
	gen ret = ret1
	replace ret    = ret2    if ret1    == . & !mi(ret2)
	gen 	job = 1 if ret1_c3 == 1
	replace job = 2 if ret2_c3 == 1
	tab 	job
	
	tab1 ret1_c ret2_c

	* multiple transitions 
	drop if ret1_c == 2 | ret2_c == 2
	* 6 cases deleted
	tab job if fallnum != fallnum[_n+1]
	for any ret1 ret2: tab X_t
	
* MISSINGS: 
	
* no employment information
tab stat welle, m
* drop if mi(stat)

count if alter < 58 & stat == 3
list fallnum alter stat if alter < 58 & stat == 3
			drop if inlist(fallnum,1201699602,1004508601,1007091202,1201287701)


egen miss1 = rowmiss(PA0800 alter depr oecd nwgr act ///
					PG1200 PG0100 )
tab miss1

for any PA0800 alter depr oecd nwgr act ///
		PG1200 PG0100 : tab X if inrange(miss,1,3) & mi(X) & !mi(job), m



sort fall welle
count if fal != fal[_n+1]

* Missing at central variables					
tab job if fallnum != fallnum[_n+1]

drop if miss1 != 0
* 1 case deleted					
tab job if fallnum != fallnum[_n+1]

tab ret if !mi(job), m
drop if mi(ret) & !mi(job)


****************************************************************************************
***! 6: Stats for the article
****************************************************************************************	
*** Count Missing Imputation
tab rm
// 275 obs with more than 1 miss on potential 6 vars
di (349 + 6 + 1) / (4110* 6) * 100


****************
* time-variables
****************

gen pre = 0
replace pre = dist if dist < 0
gen lon = 0
replace lon = dist - 2 if dist >= 3
rename ret  reth
gen ret = 0
replace ret = dist if dist > 0
replace ret = 2 if ret > 2
showcoding dist pre ret lon		

*** Summary table
tabstat PA0800 pre ret lon oecd depr nwgr act PG1200 PG0100, 	by(job) s(mean sd N) long 
tabstat sex 	if fallnum != fallnum[_n+1], 	by(job) s(mean sd N) long 
tabstat a_rs 								, 	by(job) s(mean sd N) long 


gen d_rs = dist
replace d_rs = 0 if dist < 0

save "$path_temp\ret_ds_3.dta", replace

// Graph: observation period, age at retirement, pathway	
preserve

	bys falln: egen di_l = min(dist)
	bys falln: egen di_h = max(dist)
	gen lo = a_rs + di_l if a_rs != .
	gen hi = a_rs + di_h if a_rs != .
	* list fallnum a_rs dist lo hi	 di_* in 1/50
	
	keep if !mi(a_rs)
	sample 50	
	gsort -job -a_rs -lo
	gen id2 = _n
*	replace id2 = id2 + 50 if job == 1
*	keep if job == 2
		#delimit ;

		twoway rspike lo hi id2 , 
		  xlabel()
		  ylabel("")
		  xtitle("age")
		  ytitle("observations")
		  note(" ")
		  msize(*.4)
		  horizontal
		  lwidth(vvthin)
		  || (scatter id2 a_rs , 		  msize(*.8)) 
		  , name(spike`num', replace)   legend(off) 
		  ysize(12) scale(0.8)
		;
		#delimit cr
	graph export "$path_g\desk_ars.png", as(png) replace width(500) height(1200)
	
	
restore


****************************************************************************************
***! 7: FE-Model estimation
****************************************************************************************
use "$path_temp\ret_ds_3.dta", clear
xtset fallnum welle
drop N
bys fallnum: gen N = _N
tab N
sort fallnum welle
*drop if N == 1

*
* Other cut-off values for robustness check of results
*replace nwgr = 20 if nwgr > 20 & !mi(nwgr)
*replace oecd = 2000 if oecd > 2000 & !mi(nwgr)		// di log(oecdinc)
* replace oecd = log(oecd)

*
* select only unemployed and drop invalidity pensioners and "other" before notworking
*
*gen flag = inlist(statakt,6,9,11)
*bys fallnum: egen flag2 = total(flag)
*tab flag2
*drop if flag2 != 0

sort fallnum welle
replace sex = sex - 1
replace sex = sex[_n-1] if sex != sex[_n-1] & fallnum == fallnum[_n-1]
replace westost = westost[_n-1] if westost != westost[_n-1] & fallnum == fallnum[_n-1]
gen gebjahr = intjahr - alter
tab gebjahr if fallnum != fallnum[_n-1]
gen k1 = !inrange(gebjahr,1942,1947)
replace k1 = k1[_n-1] if k1 != k1[_n-1] & fallnum == fallnum[_n-1]
tab eventy_h
gen p1 = inrange(eventy,2010,2013)
replace p1 = p1[_n-1] if p1 != p1[_n-1] & fallnum == fallnum[_n-1]



tabstat PA0800, s(mean N) by(welle)
tabstat PA0800, s(mean N) by(eventy)

* Exploration: Changes in retirement effect over period (by year of retirement)
eststo clear
for any 07 08 09 10 11 12: eststo: qui xtreg PA0800 c.ret if job == 1 & eventy == 20X, fe 
for any 07 08 09 10 11 12: eststo: qui xtreg PA0800 c.ret if job == 2 & eventy == 20X, fe 
esttab est*, stats(N_g N rho chi2 r2_o r2_b r2_w sigma corr, ///
	fmt(%9.0g %9.0g %9.2f %9.2f %9.2f %9.2f %9.2f %9.2f %9.2f )) ///
	b(%4.2f)  sfmt(%6.2f) varwidth(18)  p(%4.2f) //  p(%4.3f)
	
	
	
	
global at2	///
      at(pre == -6 ret == 0 lon == 0 ) /// 
      at(pre == -5 ret == 0 lon == 0 ) ///  
      at(pre == -4 ret == 0 lon == 0 ) /// 
      at(pre == -3 ret == 0 lon == 0 ) /// 
      at(pre == -2 ret == 0 lon == 0 ) /// 
      at(pre == -1 ret == 0 lon == 0 ) ///  
      at(pre == 0 ret == 0 lon == 0 ) /// 
      at(pre == 0 ret == 1 lon == 0 ) ///  
      at(pre == 0 ret == 2 lon == 0 ) ///  
      at(pre == 0 ret == 2 lon == 1 ) /// 
      at(pre == 0 ret == 2 lon == 2 ) ///  
      at(pre == 0 ret == 2 lon == 3 ) ///  
      at(pre == 0 ret == 2 lon == 4 )
	  
	  
****************
* FE-Models - Process of retirement
****************

	eststo: xtreg PA0800 (c.ret c.lon c.pre) PG1200 PG0100 c.oecd c.depr c.nwgr c.act  ///
				  	 if job == 2 & sex == 1 	 , fe
	margins , vsquish $at2
	marginsplot, name(g1 , replace) xdim(_atopt, nolabel) ylab(1(1)10) $gropts1 xline(7, lpattern(shortdash)) nodraw
	eststo: xtreg PA0800 (c.ret c.lon c.pre) PG1200 PG0100 c.oecd c.depr c.nwgr c.act  ///
				  	 if job == 2 & sex == 0 	 , fe
	margins , vsquish $at2
	marginsplot, name(g2 , replace) xdim(_atopt, nolabel) ylab(1(1)10) $gropts1 xline(7, lpattern(shortdash)) nodraw
	graph combine g1 g2, ycomm


eststo clear		
foreach num of numlist 1 2 {
	preserve
	* replace bil3 = 0 if bil3 == -1
	
		di " job == `num' " _n "~~~~~~~~~~~~" _n "number of events"
		count if ret == 0 & ret[_n+1] == `num' & fallnum == fallnum[_n-1]

	foreach var in depr nwgr oecd nwgr act PG1200 PG0100  { 
				di "`var' : "						
				qui sum `var'						if job == `num'
				local mean `r(mean)'	
				local sd   `r(sd)'
				di "mean = `mean'"
				replace `var' = ((`var' - `mean')) 	if job == `num' 	// grandmean-centered
				* tab `var'		if `stat'_c3 != 0		, m
			}
	
	
	tabstat depr nwgr PG1200 PG0300 alter intjahr oecdinc act if job == `num', ///
			s(n mean sd) format(%4.2f)	
			
	eststo: xtreg PA0800 (c.pre c.ret c.lon) PG1200 PG0100 ///
				  	 	 if job == `num' , fe 	
	margins, vsquish $at2
	marginsplot, name(g`num' , replace) xdim(_atopt, nolabel) ylab(1(1)10) $gropts1 xline(7, lpattern(shortdash)) nodraw
	
	eststo: xtreg PA0800 (c.ret c.lon c.pre) PG1200 PG0100 c.oecd c.depr c.nwgr c.act  ///
				  	 if job == `num' 	 , fe
	restore
	}
	graph combine g1 g2, ycomm

// Tab results	
esttab est*, stats(N_g N rho chi2 r2_o r2_b r2_w sigma corr, ///
	fmt(%9.0g %9.0g %9.2f %9.2f %9.2f %9.2f %9.2f %9.2f %9.2f )) ///
	b(%4.2f)  sfmt(%6.2f) varwidth(18)  se(%4.2f) //  p(%4.3f)
// Save results
qui estout est* using "$path_g\results.csv"	, replace ///
		cells(b(star fmt(%9.2f)) se(par)) stats(N_g N rho corr bic chi2 r2_o r2_b r2_w sigma, ///
		fmt(%9.0g %9.0g %9.2f %9.2f %9.2f %9.2f %9.2f %9.2f %9.2f %9.2f)) ///
		legend collabels(none) varlabels(_cons Constant) //label 


****************
* FE-Models - Differences
****************		
replace westost = westost - 1
		
eststo clear	
foreach num of numlist 1 2 {
	
	preserve
	* replace bil3 = 0 if bil3 == -1
	
		di " job == `num' " _n "~~~~~~~~~~~~" _n "number of events"
		count if ret == 0 & ret[_n+1] == `num' & fallnum == fallnum[_n-1]

	foreach var in depr nwgr oecd nwgr act PG1200 PG0100  { 
				di "`var' : "						
				qui sum `var'						if job == `num'
				local mean `r(mean)'	
				local sd   `r(sd)'
				di "mean = `mean'"
				replace `var' = ((`var' - `mean')) 	if job == `num' 	// grandmean-centered
				* tab `var'		if `stat'_c3 != 0		, m
			}
	
	
	tabstat depr nwgr PG1200 PG0300 alter intjahr oecdinc act if job == `num', ///
			s(n mean sd) format(%4.2f)	
	
	foreach var in bil2 sex westost k1 p1 {
		eststo: qui xtreg PA0800 `var'#(c.ret) PG1200 PG0100 c.oecd c.depr c.nwgr c.act  ///
						if job == `num'   	 , fe
						}
	restore
	}
	* graph combine g1 g2, ycomm

// Tab results	
esttab est* , stats(N_g N rho chi2 r2_o r2_b r2_w sigma corr, ///
	fmt(%9.0g %9.0g %9.2f %9.2f %9.2f %9.2f %9.2f %9.2f %9.2f )) ///
	b(%4.2f)  sfmt(%6.2f) varwidth(18)  p(%4.2f) //  p(%4.3f)
// Save results
qui estout est* using "$path_g\results2.csv"	, replace ///
		cells(b(star fmt(%9.2f)) se(par)) stats(N_g N rho corr bic chi2 r2_o r2_b r2_w sigma, ///
		fmt(%9.0g %9.0g %9.2f %9.2f %9.2f %9.2f %9.2f %9.2f %9.2f %9.2f)) ///
		legend collabels(none) varlabels(_cons Constant) //label 
		
		
		
/* Mundlak-Method */
egen m_ret1=mean(ret1), by(fallnum)
egen m_welle=mean(welle), by(fallnum)
gen IA = bil2*ret1
egen m_IA=mean(IA), by(fallnum)

*replace bil2 = bil2 + 1
egen m_bil2=mean(bil2), by(fallnum)
egen id = group(fallnum) 

* egen m_siops = mean(siopslewt), by(fallnum) 

preserve
keep if inrange(alter,57,66)
qui eststo fe: xtreg PA0800 c.welle c.bil3##i.ret1, fe
qui eststo re: xtreg PA0800 c.welle m_welle c.bil2##i.ret1 m_ret1 m_IA m_bil2 , re
eststo mu: mundlak PA0800 welle bil3 ret1 IA, hybrid
esttab fe re 
		

* log close
clear
exit
