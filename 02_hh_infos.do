
preserve
#del ;
global keepvars3 
hnr welle
sample 
blneualt
hhincome
oecdincn
depindg2;
#del cr

use $keepvars3 using "$path_in\HHENDDAT.dta", clear
save "$path_temp\HH.dta", replace

restore

preserve
use hnr welle psu strpsu using "$path_in\hweights.dta", clear
sort hnr welle
save "$path_temp\psuinfo", replace
